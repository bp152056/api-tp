const express = require('express');
const app = express();
const movies = require('./movies');
const PORT = process.env.port || 3000;
const API_KEY = 'f4b04396';
const axios= require('axios');

app.use(express.json());

// Root request
app.get('/', (req, res) => {
    res.send('TP API');
});

// Get all movies (of movies.js)
app.get('/movies', (req, res) => {
    res.json(movies);
});

// Get movie by id
app.get('/movies/:id', (req,res) => {
    const movie = movies.find(c => c.id === parseInt(req.params.id));

    //404 not found
    if(!movie) res.status(404).send('The movie with the given ID was not found');
    res.json(movie);
});

// Post new movie, id set auto 
app.post('/movies', (req,res) => {
    if(!req.body.name || req.body.name.length < 2){
        //400 bad request
        res.status(400).send('Name is required or must be at least 2 character long');
        return;
    }
    const movie = {
        id: movies.length + 1, 
        name: req.body.name
    };
    movies.push(movie);
    res.send(movie);
});

// Modify movie name by its id 
app.put('/movies/:id', (req,res) => {
    const movie = movies.find(c => c.id === parseInt(req.params.id));
    if(!movie) res.status(404).send('The movie with the given ID was not found');

    if(!req.body.name || req.body.name.length < 2){
        res.status(400).send('Name is required or must be at least 2 character long');
        return;
    }
    movie.name = req.body.name;
    res.send(movie);
});

// Delete movie by its id
app.delete('/movies/:id', (req,res) => {
    const movie = movies.find(c => c.id === parseInt(req.params.id));
    if(!movie) res.status(404).send('The movie with the given ID was not found');

    const index = movies.indexOf(movie);
    movies.splice(index, 1);
    res.send(movie);
});

// Return movie data from omdb api when title of movie in url as query (ex: localhost:3000/axios?name=inception)
app.get('/axios', (req,res) => {    
    axios.get(`http://www.omdbapi.com/?t=${req.query.name}&apikey=${API_KEY}`).then(result => {
        res.send(result.data);
        return;
    });
});

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));